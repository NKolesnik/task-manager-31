package ru.t1consulting.nkolesnik.tm.client;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

@Getter
@Setter
public abstract class AbstractEndpoint {

    @NotNull
    private final String host = "localhost";

    @NotNull
    private final Integer port = 6060;

    @Nullable
    private Socket socket;

    @SneakyThrows
    public void connect(){
        socket = new Socket(host, port);
    }

    @SneakyThrows
    public void disconnect(){
        if(socket==null) return;
        socket.close();
    }

    @NotNull
    @SneakyThrows
    protected Object call(@NotNull Object data){
        getObjectOutputStream().writeObject(data);
        return getObjectInputStream().readObject();
    }

    @NotNull
    @SneakyThrows
    private ObjectOutputStream getObjectOutputStream(){
        return new ObjectOutputStream(getOutputStream());
    }

    @NotNull
    @SneakyThrows
    private ObjectInputStream getObjectInputStream(){
        return new ObjectInputStream(getInputStream());
    }

    @Nullable
    @SneakyThrows
    private OutputStream getOutputStream(){
        if(socket == null) return null;
        return socket.getOutputStream();
    }

    @Nullable
    @SneakyThrows
    private InputStream getInputStream(){
        if(socket == null) return null;
        return socket.getInputStream();
    }

}
