package ru.t1consulting.nkolesnik.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.endpoint.ISystemEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.request.ServerAboutRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.ServerVersionRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.ServerAboutResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.ServerVersionResponse;

public final class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Override
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @NotNull
    @Override
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ServerAboutResponse aboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(aboutResponse.getEmail());
        System.out.println(aboutResponse.getName());
        @NotNull final ServerVersionResponse versionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(versionResponse.getVersion());
        client.disconnect();


    }

}
