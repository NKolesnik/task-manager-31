package ru.t1consulting.nkolesnik.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.dto.request.ServerAboutRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.ServerVersionRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.ServerAboutResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);
}
