package ru.t1consulting.nkolesnik.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.component.Server;

public abstract class AbstractServerTask implements Runnable{

    @NotNull
    protected Server server;

    public AbstractServerTask(Server server) {
        this.server = server;
    }

}
