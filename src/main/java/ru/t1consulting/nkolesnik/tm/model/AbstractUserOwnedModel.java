package ru.t1consulting.nkolesnik.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class AbstractUserOwnedModel extends AbstractModel {

    @NotNull
    private String userId;

}
