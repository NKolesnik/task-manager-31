package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

}
