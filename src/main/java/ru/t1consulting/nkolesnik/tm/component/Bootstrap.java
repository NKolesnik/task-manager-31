package ru.t1consulting.nkolesnik.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1consulting.nkolesnik.tm.api.endpoint.ISystemEndpoint;
import ru.t1consulting.nkolesnik.tm.api.repository.ICommandRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.IProjectRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.IUserRepository;
import ru.t1consulting.nkolesnik.tm.api.service.*;
import ru.t1consulting.nkolesnik.tm.command.AbstractCommand;
import ru.t1consulting.nkolesnik.tm.command.system.ExitCommand;
import ru.t1consulting.nkolesnik.tm.dto.request.ServerAboutRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.ServerVersionRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.ServerAboutResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.ServerVersionResponse;
import ru.t1consulting.nkolesnik.tm.endpoint.SystemEndpoint;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.exception.system.ArgumentNotSupportedException;
import ru.t1consulting.nkolesnik.tm.exception.system.CommandNotSupportedException;
import ru.t1consulting.nkolesnik.tm.repository.CommandRepository;
import ru.t1consulting.nkolesnik.tm.repository.ProjectRepository;
import ru.t1consulting.nkolesnik.tm.repository.TaskRepository;
import ru.t1consulting.nkolesnik.tm.repository.UserRepository;
import ru.t1consulting.nkolesnik.tm.service.*;
import ru.t1consulting.nkolesnik.tm.util.SystemUtil;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1consulting.nkolesnik.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @NotNull
    private final Server server = new Server(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class,systemEndpoint::getVersion);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) {
            new ExitCommand().execute();
        }
        prepareStartup();
        while (true) processCommand();
    }

    private void processCommand() {
        try {
            System.out.println("Enter command:");
            @NotNull final String command = TerminalUtil.nextLine();
            processCommand(command);
            System.out.println("[OK]");
            loggerService.command(command);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            System.err.println("[FAIL]");
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void prepareStartup() {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        fileScanner.start();
        server.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        backup.stop();
        server.stop();
    }

    private void initDemoData() {
        userService.create("test", "test", "test@test.email.ru", Role.USUAL);
        userService.create("admin", "admin", "admin@admin.email.ru", Role.ADMIN);
    }

    protected void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    protected void processCommand(@Nullable final String command, boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) {
            return false;
        }
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

}


